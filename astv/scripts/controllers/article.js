'use strict';

angular.module('gnnApp')
  .controller('ArticleCtrl', ['$scope', 'SiteData', function ($scope, SiteData) {
	  $scope.currentPage = 0;
	  $scope.pageLimit = 20;
	  $scope.articles = SiteData;
	  
	  // Methods
	  $scope.ShowArticle = function(index){
		  for (var i=0; i<SiteData.length; i++){
			  if (SiteData[i].id == index){
				  $scope.article = SiteData[i];
			  }
		  }
	  };
	  
	  $scope.ReadArticle = function(index){
		  window.location = "article.html?id=" + index;
	  };
	  
	  // Search
	  $scope.query = '';
	  
	  // Init
	  $scope.ShowArticle(window.location.search.substring(4));	  
  }]);