'use strict';

angular.module('gnnApp')
  .controller('MediaCtrl', ['$scope', 'SiteData', function ($scope, SiteData) {
	  $scope.activeMedia = { active: false, media:null};
	  $scope.hasVideos = false;
	  $scope.hasAudio = false;
	  
	  $scope.videoArticles =  [];
	  $scope.audioArticles = [];
	  
	  $scope.currentPage = 0;
	  $scope.pageLimit = 6;
	  
	  // Search
	  $scope.query = '';
	  
	  // Methods
	  $scope.ShowArticle = function(index){
		  $scope.article = SiteData[index];
	  };
	  $scope.ReadArticle = function(index){
		  window.location = "article.html?id=" + index
	  };
      $scope.PlayVideo = function(file, title){
          $scope.media_filename = file;
          $scope.title = title;
      };
	  
	  // Data Manipulation
        $scope.getVideoMedia = function(){
            for (var i=0; i<SiteData.length; i++){
                if (SiteData[i].media_type == 'video'){
                    $scope.videoArticles.push(SiteData[i]);
                }
            }
            $scope.hasVideos = ($scope.videoArticles.length > 0);
            if ($scope.hasVideos){
                $scope.media_filename = $scope.videoArticles[0].media_filename;
                $scope.title = $scope.videoArticles[0].title;
            }
        }
	  
	  $scope.getAudioMedia = function(){
		  for (var i=0; i<SiteData.length; i++){
			  if (SiteData[i].media_type == 'audio'){
				  $scope.audioArticles.push(SiteData[i]);
			  }
		  }
		  $scope.hasAudio = ($scope.audioArticles.length > 0);
	  }
	  // Init();
	  $scope.getVideoMedia();
	  $scope.getAudioMedia();
	  
  }]);