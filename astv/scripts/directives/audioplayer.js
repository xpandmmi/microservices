'use strict';

angular.module('gnnApp')
  .directive('audioPlayer', function () {
    return {
      restrict: 'E',
	  controller: function($scope){
		  $scope.Reset = function(){
			  console.log('reset');
		  }
	  },
	  scope: { title: '@', filename: '@', callback:'&' },
      link: function(scope, element, attrs){
		  scope.isPlaying = false;
		  scope.Play = function(){
			  scope.isPlaying = !scope.isPlaying;
			  scope.callback();
		  }
	  },
	  template: '<div class="media-body"><h4 class="media-heading">{{title}}</h4></div><div class="col-xs-4 Player Player-Button" ng-click="Play()"><p class="glyphicon glyphicon-play"></p></div><div class="col-xs-4 Player Player-Progress">{{filename}}</div><div class="col-xs-4 Player Player-Timer"></div>'
    };
  });