'use strict';

angular.module('gnnApp')
  .directive('siteNavigation', ['SiteData', function (SiteData) {
    return {
      template: '<ul></ul>',
      restrict: 'E',
	  replace: true,
      compile: function (tElement, tAttrs, transclude) {
		  var categories = [];
		  
		  var FilterArray = function(value, index, self){
			  return self.indexOf(value) === index;
		  }
		  
		  for (var i=0; i < SiteData.length; i++){
			  var category = SiteData[i].category;
			  categories.push(category);
		  }
		  
		  var categories = categories.filter(FilterArray);
		  
		  return function(scope, element, attrs){
			  var navBar = "";
        navBar += '<li><a href="/twitter"><img src="images/TwitterLogo.png" width="20" /></a></li>';
        
			  for (var i=0; i < categories.length; i++){
				  navBar += '<li><a href="category.html?cat=' + categories[i] + '">' + categories[i].toUpperCase() + '</a></li>'
			  }
			  element.html(navBar);
		  }
      }
    };
  }]);
