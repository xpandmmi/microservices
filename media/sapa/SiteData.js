'use strict';

angular.module('gnnApp')
	.service('SiteData', function Sitedata() {
		return 	[
        {
        "id": "13",
        "timestamp": "June-01-2015",
        "published": "0",
        "category": "local",
        "title": "NATO Murders Innocent Atropians, Doesn\u2019t Understand SAPA",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Reports today confirmed that NATO forces have begun targeting all SUV&rsquo;s. NATO believes <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Arianian Special Purpose Forces (SPF) <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">to be operating in SUV&rsquo;s and instead of confirming their targets they have <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">begun a murderous campaign of shooting anyone operating a SUV.<\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">These actions show that NATO is not here to help our people our people, NATO forces do not care about the lives of Atropians. All NATO cares about is purging our land of anyone that stands in their way. They view all Atropians as enemies and will not hesitate to kill our people. No Atropian family is safe as long as the NATO forces are barreling through our streets blindly murdering our fellow Atropians. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Additionally NATO is accusing SAPA of working with SPF. Nothing could be further from the truth. Where are you getting your intelligence NATO? Dimetz? Of course he would accuse SAPA of forming this relationship. He is seeing his country turn against his corrupt and treacherous ways and he, like all dictators, will stop at nothing to remain in power. SAPA repeatedly runs SPF out of our towns. We know that SPF will draw the attention of NATO, and NATO has already demonstrated their lack of care for Atropian wellbeing. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">SAPA will not tolerate the blind murder of our families and children. These horrendous actions and accusations against our people must be stopped. SAPA demands that the ravenous dogs that engage&nbsp;innocent civilians be brought to justice for their crimes. SAPA is not SPF. SPF is not SAPA. Join SAPA and learn the difference.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X6X1.png",
        "media_type": "image",
        "media_quote": ""
    },{
        "id": "14",
        "timestamp": "June-02-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA Declaration",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">People of South Atropia, due to NATO&rsquo;s callousness throughout their time in our homeland, as evidenced yesterday in Milesov, we must declare ourselves against their presence. Shamelessly they let the men, women, and children die in Milesov, rather than risk their own precious soldiers in preventing a chemical spill that has killed a large number of South Atropians, wounded more, and destroyed the environment in Milesov. NATO! Stay away from Milesov and our people, or you will regret it! We are prepared to use force to secure our town. You have been warned.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X7X2.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "12",
        "timestamp": "May-31-2015",
        "published": "0",
        "category": "local",
        "title": "Fires Destroy Livelihoods in Milesov",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Fires destroy crops in and around Milesov as NATO forces unnecessarily occupy the town. Machines of war barrel through town, frightening women and children. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Local farmers had to sit by and watch the fruits of their labor burn to ashes. One farmer, Ali Dagestani, says he will have trouble feeding his family due to the loss of his only income. Our members organized an impromptu donation and were able to secure enough resources to get the Dagestanis through the month. But what happens next month? <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">SAPA is here for you Ali, we are here for all south Atropians. Join SAPA, help us end the suffering and rid our lands of these negligent war fighters.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X5X3.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "11",
        "timestamp": "May-31-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA is the Future",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today, the Mayor officially appointed another member of SAPA to the Milesov police force. SAPA is committed to ensuring the safety of the people of Milesov. However, a local Pro-RPD judge is trying to stop the political appointments. This would reduce the police force and threaten the security and well being of all Milesov citizens. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">SAPA delivered much needed funds to a local school today. The school, which the community has been struggling to support due to many roads and trade routes being damaged from the fighting, will receive increased academic opportunities. SAPA cares about the future of the Atropian children and can provide the resources to achieve it. The local government cannot even protect the infrastructure of the town let alone, provide a future for our children. Join SAPA today and ensure a brighter future for all Atropians!&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X5X2.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "10",
        "timestamp": "May-31-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA Aid Becomes the Norm",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">When the Atropian government fails you, trust in SAPA to provide for your essential needs. If you doubt our abilities, look no further than the grateful citizens of Boyat and the Internally displaced persons who received much needed medical supplies, food, water, tents, and blankets.<\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">All of these were provided by TPP and delivered \/ distributed by SAPA. If you still doubt us, ask the smiling children in Milesov who just received much needed school supplies. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Unfortunately, SAPA does not have the equipment nor trained personnel to clear the minefields placed by NATO forces in our children<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rsquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">s back yards. Minefields, such as the one north of Milesov are hindering the economic recovery of South Atropia. Goods are not able to be shipped, merchants fear for their safety and will not approach the cities. NATO has crippled the economic recovery of South Atropia. Join SAPA and help us rebuild our land to its former glory. SAPA will pave the way to a prosperous future for all Atropians.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X5X1.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "9",
        "timestamp": "May-29-2015",
        "published": "0",
        "category": "local",
        "title": "United We Stand",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Brothers and sisters of South Atropia, forward progress is being made in the beautiful city of Belake. Recently a new, democratically elected mayor was sworn in. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The mayor, a TPP member, endorses SAPA as the town<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rsquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">s most valuable asset. Together with SAPA he promises to keep Belake clear of NATO forces. SAPA will not let him down. Already SAPA rallied the townspeople to block a known NATO route through the city. Despite being fired upon, the block held, but the shots caused wide spread panic. The excited crowd soon calmed down at the direction and leadership of a joint SAPA\/police presence. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Many IDPs arrived in Belake yesterday and received a warm welcome from everyone. SAPA and TPP joined forces again to distribute much needed medical supplies. The IDPs are in good hands and more will be done soon to ease their suffering. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Forward progress continues to send a beacon of hope throughout this war torn land. Better days are on the horizon. South Atropians have united and will remain so for years to come.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "x3x10.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "8",
        "timestamp": "May-28-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA Saves Man\u2019s Life",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Brothers and sisters of South Atropia, today, at the request of our crooked government, fury rained from the skies in the form of landmines. The multinational coalition known as NATO fired an artillery delivered ammunition known as FASCAM. These awful munitions scatter hundreds of landmines over an area to prevent an enemy from moving freely. However, this minefield never came near the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">enemy<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rdquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">, unless you call an innocent man returning to his family the enemy. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today our members came upon a gruesome discovery. A local resident, traveling from Pasron City to Boyat struck this monstrous minefield in his vehicle. You could hear the explosion from Boyat. Concerned citizens and SAPA members rushed to the area where the blast originated only to find the man hanging on to his life by his fingernails. The quick reaction by our trained members saved his life, that and the nearby Arianian field hospital. We did not intend to have the Arianians treat him, but he had lost too much blood to travel any further. Our brother might not have made it without that hospital. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Our government welcomed NATO with hopes of peace and prosperity, yet all the Atropian people know is suffering and hardship. Meanwhile our enemy provides more care to a citizen in need than our own institutions. We will not stand for this and you shouldn<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rsquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">t either.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X2X4.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "7",
        "timestamp": "May-27-2015",
        "published": "0",
        "category": "local",
        "title": "Our Way, Their Way",
        "body": "<p>The other day I overheard two gentlemen talking in the market. They were discussing an article they had read on this website, specifically the call to action.&nbsp;<\/p>\n\n<p>One said, &ldquo;well I&rsquo;m already doing my part, I&rsquo;m a proud supporter of The Peoples Party and believe they can make a difference.&rdquo; Now, where I do not disagree, The Peoples Party has some opinions and ideas that resonate well with our own visions--it&rsquo;s the getting there we disagree on.<\/p>\n\n<p>As popular as the TPP is, you won&rsquo;t win any polls in a NO WIN election. The Republican Party of Democracy is anything but democratic, and they own the polls, they own the game.<\/p>\n\n<p>So excuse my absence on Election Day, I&rsquo;ll leave that to you dreamers and well wishers. You&rsquo;ll find the South Atropian People&rsquo;s Army on the front lines, ensuring justice by any means necessary.&nbsp;<\/p>\n",
        "media_filename": "X1X5_updated.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "6",
        "timestamp": "May-27-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA on NATO",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In these, the final days of the Republican Party for Democracy (RPD) and their corrupt regime, the so- called president Dimetz has called for international assistance to resist the Arianian invasion. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The North Atlantic Treaty Organization (NATO) has, under UN auspices, consented to do so. NATO has their own reasons for defending Cela&nbsp;and fighting Ariana; they enjoy the cheap energy that Dimetz steals from the Atropian people, and do not wish to lose it. But that has nothing to do with the people of South Atropia, the people to whom the South Atropian People&rsquo;s Army (SAPA) is committed. NATO will leave when this <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">fight is over, and SAPA has no wish for them to stay. The tyrant Dimetz and his RPD cronies are the true enemies of SAPA. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">People of South Atropia, stay strong during these times of trouble, stay loyal to the sons, brothers, and fathers in SAPA, and stay informed through this website.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X1X4.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "4",
        "timestamp": "May-07-2015",
        "published": "0",
        "category": "local",
        "title": "Mother Sasha is Calling...",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Three years ago, Emperor Dimetz brutally martyred hundreds of peaceful Atropian brothers and sisters in Krupnikas exercising their right to be heard! Our bloody Sunday. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Sofia Sasha went to the market in Krupnikas to buy food for her hungry family. As a virtuous and caring mother of five beautiful South Atropian children, she rarely had enough money to survive. As a widow, she was the only source of income for her family working as a maid for a brutal official of the Republican Party of Democracy (RPD). Despite being mistreated, she still respected the central government and hoped that they would solve many of the problems facing the people. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">While at the market that fateful Sunday, a black-booted thug in uniform struck her in the face with his baton. She awoke on the ground to a barrage of gunfire witnessing hundreds of men and women fall to <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the ground. The emperor&rsquo;s thugs shot indiscriminately into the town square, injuring and killing peaceful <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">protestors and passersby like Sofia. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Sofia not only woke up to gunfire, but woke up to the injustice and brutal hand of the Emperor and his RPD minions! Today, Sofia is one of our most victorious commanders of the South Atropian People&rsquo;s Army (SAPA). Mother Sasha not only cares for her children but to all the subjugated people of South Atropia. She will not only weep for you, but will fight for you! <\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Listen brother! If a widow and mother of five has the courage to pick up a Kalashnikov to defend her <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">children&rsquo;s future, so must you. Brothers in <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Nerst, Pasron, Joynaria, Velaz, Deaon, Reyill and Farscin <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">hear her call! <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Remember Bloody Sunday!&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D19.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "3",
        "timestamp": "May-12-2015",
        "published": "0",
        "category": "local",
        "title": "From Molotovs to Complex Attacks: SAPA Fights On!",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In the past, the brothers and sisters <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">of the South Atropian People&rsquo;s Army (SAPA) <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">accomplished their first <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">triumphant complex attack targeting the emperor&rsquo;s army of fools and guardians of injustice. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">This <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">complex attack was staged along routes routinely patrolled by imperial Army of the Republican Party of Democracy in Velaz Province. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Our courageous warriors used multiple tactics to ensure success by laying improvised mines along designated kill zones. Once the empe<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">ror&rsquo;s thugs were in our crosshairs, we <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">detonated the mines and simultaneously used rocket propelled grenades, Kalashnikovs and all the weapons in our disposal to destroy the enemy. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">We will continue using these and other proven methods to be victorious.<\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">For a step-by-step instructions to build your own mine, watch the video.<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D18.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "2",
        "timestamp": "May-12-2015",
        "published": "0",
        "category": "local",
        "title": "The Corrupt Government of Cela",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Our government robs its people of their money, pride, and freedom, then turns its back on them in their <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">time of need. Who would stand by such a government? Who would stand and say, &ldquo;They need to be running our lives?&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Not <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the South Atropian People&rsquo;s Army (SAPA)<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">. We are dedicated to an independent South Atropia, free of the corrupt influence of the Republic Party of Democracy (RPD) government in Cela. The RPD flunkeys are only concerned with their own welfare, as they showed when they turned tail and ran when the Arianian forces struck. We, however, are willing to fight for our freedom and the right to self-determination. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">We are SAPA. We will fight the den of thieves that is RPD, and we will win. Will you join us?&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D17_sapa_corrupt.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "1",
        "timestamp": "May-12-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA: Why We Fight!",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">With our poor country now the battleground between two major armies, we feel it is time for the South <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Atropian People&rsquo;s Army <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">(SAPA) to properly declare to those who have chosen to insert themselves in our unhappy country our demands, our vision and our goals for the future. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">This moment is taken for the greatest solemnity with our people being brutalized by craven Government of Atropia and the historic antagonist to our south, the Arianian Defence Force. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">What do we want? This is a question typically asked by lazy and stupid Western press as well as the Arianian press corps who ought to know better by now. What do we want? We want what all nations around the world aspire to have: political independence. It is fitting that in this centenary of the First World W<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">ar whereby the American President Woodrow Wilson claimed to &lsquo;make the world safe for democracy&rsquo; and whose 14 Points endowed all nations with the right of self<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">-determination that we, the people of South Atropia, assert our rights for our own place in the sun and to join the community of <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">nations. The list of crimes committed by the regime calling itself the &lsquo;Government of Atropia&rsquo; is too <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">numerous and too sordid to be recalled in this brief space. Our displeasure with the litany of abuse, neglect, corruption and hopelessness can be summed in four simple words: WE DO NOT CONSENT. We do not consent to this gang of thieves calling itself a &lsquo;government&rsquo;. We do not consent to its discrimination. We do not consent to its neglect. We do not consent to the mafia known as the Republican Party of Democracy. We do not consent when there is a better alternative. <\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">And there is indeed a better alternative. The South Atropian People&rsquo;s Party is that alternative. We are <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">ready, willing and able to meet the challenge of our times and bring a better solution that represents ALL of the people of South Atropia and not just the privileged elite that are currently served by the patently corrupt Dimetz regime. We say to the people of South Atropia this: stand fast. You will know the SAPA by our fruits. We will do what we say we will do to restore honour and decency to good governance. We understand your needs and we will provide them. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">And to the warring interlopers from foreign lands we say this to you: back off. This is not your fight. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">To NATO: You have been brought here to restore a criminal enterprise masquerading as a government. Be very wary in your attempts to interject yourselves into our problems. If your goal here is to restore the status quo antebellum, then that goal <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">is immoral and illegal. Don&rsquo;t tread on us. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">To Ariana: You are the epitome of the historical bad neighbour. We know all too well what you desire in our country and you have no place here. Return to your homes over the border, leave us in peace, help us in our cause and become good neighbours with the new South Atropian Republic that, while only in its infancy now, will soon become a well respected member of the community of states.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D16.jpg",
        "media_type": "image",
        "media_quote": ""
    }
]});
