'use strict';

angular.module('gnnApp')
  .service('Slider', function Slider() {
	  return [
	  	{
	  		image: 'ub_001.jpg',
			quote: ''
	  	},{
	  		image: 'slide1.png',
			quote: ''
	  	},{
	  		image: 'ub_002.jpg',
			quote: ''
	  	},{
	  		image: 'slide2.png',
			quote: ''
	  	},{
	  		image: 'ub_003.jpg',
			quote: ''
	  	},{
	  		image: 'slide5.png',
			quote: ''
	  	},{
	  		image: 'ub_004.jpg',
			quote: ''
	  	},{
	  		image: 'slide6.png',
			quote: ''
	  	}
	  ];
  });
