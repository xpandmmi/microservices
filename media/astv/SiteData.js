'use strict';

angular.module('gnnApp')
	.service('SiteData', function Sitedata() {
		return 	[
    {
        "id": "67",
        "timestamp": "June-01-2015",
        "published": "0",
        "category": "local",
        "title": "Chemical Attack in Milesov!",
        "body": "<div title=\"Page 1\">\n<div>\n<div>\n<p>Reports continue to trickle into ASTV alleging that persistent chemical agents have been used in today in Milesov.<\/p>\n\n<p>Early in the day there were rumors circulating wildly throughout the Province that chemical weapons of this type were going to be used somewhere in Pasron province. At that time ASTV sources were unable to confirm their presence or the potential location for their use.<\/p>\n\n<p>Shortly before noon the Mayor of Milesov reported that several patrons of a local restaurant were showing signs of distress- coughing, watery eyes and extreme intestinal cramping. It was unclear as to whether their symptoms were related to poorly prepared food, or signs of something more sinister.<\/p>\n\n<p>Sadly, it appears that these symptoms were in fact an early warning of something far more catastrophic. ASTV reporters are unable to enter Milesov at this time to confirm the rumors, as the magnitude or range of the contamination is unknown. Survivors have been able to reach out to family and friends by phone and are reporting that thousands are dead or seriously sick. The Mayor, while injured, is alive, but the Police Chief was not so lucky.<\/p>\n<\/div>\n<\/div>\n<\/div>\n\n<div title=\"Page 2\">\n<div>\n<div>\n<p>The fate of international aid workers, who had been expecting to organize a relief convoy of supplies from Milesov to Belake, is unknown at this time as well.<\/p>\n\n<p>As of this report no single group has claimed responsibility for this heinous act. ASTV will bring more information as it becomes available.&nbsp;<\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X6X4.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "66",
        "timestamp": "June-01-2015",
        "published": "0",
        "category": "local",
        "title": "World Vision to Resume Operations in Pasron Province",
        "body": "",
        "media_filename": "X6X2.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "65",
        "timestamp": "June-01-2015",
        "published": "0",
        "category": "local",
        "title": "Mysterious Illness in Milesov",
        "body": "",
        "media_filename": "CBI_2431.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "61",
        "timestamp": "May-30-2015",
        "published": "0",
        "category": "local",
        "title": "World Vision Delivers Supplies in Boyat",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today in Boyat, World Vision handed out much needed supplies to areas in the midst of heated conflict.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X4X4.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "59",
        "timestamp": "May-30-2015",
        "published": "0",
        "category": "local",
        "title": "IDPs flee East, South Atropia \u2013 Romanians Offer No Assistance",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Dozens of Internally Displaced People (IDPs) from southern and eastern Atropia made their way through Milesov on their way to the IDP camp in Belake on Friday. The group originated from Holtri and gained in size passing through Noust and Boyat. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;I was wal<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">king through the fields and there was a huge <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">explosion and I fell down,&rdquo; said Sahr Yakab Vadahazch, a farmer from outside of Boyat. &ldquo;I woke up hours later with a gash on my head, I don&rsquo;t know who caused the explosion but I was afraid so I left.&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Locals from Boyat said that the village was taken over by Arianian tanks forcing the villagers from their homes. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;We are going to Belake, we heard there was a camp there,&rdquo; said Vadahazch. &ldquo;We need shelter, food, water, and care.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Just outside of Milesov, the IDPs ran into 1st Company of the 151st Romanian Infantry Battalion. The people pleaded for food and water but the Romanians would not give any aid. When pressed about the large water tank located on the hill, the Romanian leader, who would not give his name, said it was empty. The mood quickly became hostile as IDPs and Romanians began arguing. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;Why are you in our country if you aren&rsquo;t going to help us?&rdquo; said one of the IDPs. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">As the crowd angrily dispersed one of the IDPs shouted expletives at the Romanian s<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">oldiers and another was heard saying, &ldquo;You&rsquo;re worse than the Arianians!&rdquo;&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The local people are unsure of who to trust when it seems like both NATO and Arianian forces are unwilling to help them.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X4X3.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "53",
        "timestamp": "May-29-2015",
        "published": "0",
        "category": "local",
        "title": "Milesov Mayor Appeals for Help",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In Milesov, many townspeople were huddled in their homes in hopes of staying out of harm<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rsquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">s way. There were, however, several brave individuals who walked the deserted streets including the police chief and the mayor. Mayor Ayna Sultanova expressed her desire to be in contact with NATO forces. The <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">town&rsquo;s communication network had been knocked out during the fighting and she had not been <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">contacted by the NATO coalition. Meanwhile, Mayor Sultanova has asked <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">her citizens to &ldquo;be paitent and stay in their houses.&rdquo; She remained hopeful of aid from both the government and NGOs in the very near <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">future once the security situation allows it. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Police Chief Sabit Orujov called the situation a &ldquo;tragedy&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">and noted that his minimal resources left him unequipped to deal with the internally displaced persons coming through Milesov. Chief Orujov confirmed that he had not been in contact with either the Atropian government or the NATO coalition. He expressed his willingness to accept training, equipment, and help from anyone who would offer it. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;Security is the first priority,&rdquo; said Chief Orujov. He also mentioned fears of further instability if fighting <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">in the area disrupted the major supply route running from the west through Milesov.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "X3X9.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "45",
        "timestamp": "May-28-2015",
        "published": "0",
        "category": "local",
        "title": "Interview with TPP Spokesperson on the TPP\u2019s Declaration of Autonomy",
        "body": "",
        "media_filename": "X2X1.png",
        "media_type": "audio",
        "media_quote": ""
    },
    {
        "id": "42",
        "timestamp": "May-27-2015",
        "published": "0",
        "category": "local",
        "title": "Mines Reported Near Noust",
        "body": "",
        "media_filename": "X1X2.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "41",
        "timestamp": "May-27-2015",
        "published": "0",
        "category": "local",
        "title": "TPP Declares Autonomy",
        "body": "<p>TPP Declares Autonomy&nbsp;<\/p>\n",
        "media_filename": "CBI_2063.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "35",
        "timestamp": "May-25-2015",
        "published": "0",
        "category": "local",
        "title": "NGO's Speak With ASTV",
        "body": "<p>NGO&#39;s Speak With ASTV<\/p>\n",
        "media_filename": "E7E2-NGO_Statements.png",
        "media_type": "video",
        "media_quote": ""
    },
    {
        "id": "28",
        "timestamp": "May-22-2015",
        "published": "0",
        "category": "local",
        "title": "World Vision Forced to Suspend Social Worker Program in Milesov",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">World Vision announced today that it has been forced to suspend temporarily its program to support Social Workers in Milesov. The decision came after careful review and extensive consultation with staff on the ground in Pasron <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Province and in consultation with the US Embassy, as well as World Vision&rsquo;s <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">own security team. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;We can no longer assure the safety of our team,&rdquo; said <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Holly Barnes, the Regional Representative for World Vision. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;It is out of an abundance of caution <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">that we are recalling the team to Cela. It is our sincere hope that there will be a swift and peaceful resolution to this conflict and that we can again <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">return to this important work,&rdquo; she said. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Domestic violence is not uncommon in Atropia, where up to 60 per cent of women are reported to experience some form of gender-based violence, ranging from inability to visit a doctor or use <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">contraception without husband&rsquo;s permission to physical abuse, resulting in injuries and even death. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">According to the 2010 Atropian Demographic and Health Survey, more than half the men in Atropia consider wife beating acceptable.&nbsp;<\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The position and concept of the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;Social Worker&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">is relatively new in Atropia. World Vision introduced role of the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;Social Worker&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">several years ago in other Atropian communities where World Vision has Area Development Programs (ADPs), and the concept in Pasron Province has already proven its effectiveness. The Social Workers not only work to strengthen community-level child protection mechanisms, they also help prevent domestic violence and child institutionalization. At the same time, Social Workers providing support to families and children in difficult circumstances and advising them on opportunities to improve their economic situations. Over the past two years, the Social Workers in communities where World Vision is active have helped to address domestic violence in more than 50 families. World Vision is currently advocating for the establishment of a national system of social works that would ensure the presence of Social Workers in every community. <\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Social Worker programs in other areas of the country will continue uninterrupted. Meanwhile, World Vision remains committed to the people of Atropia.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "E4E1.jpg",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "27",
        "timestamp": "May-21-2015",
        "published": "0",
        "category": "local",
        "title": "Ariana Uses Chemical Weapons",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Late yesterday it was reported that Arianian troops used chemical weapons in Reyill Province near the Atropian border with Ariana, poisoning Atropian soldiers and civilians living in the area. Initial tests indicate that the chemical used was some type of nerve agent, but definitive results are expected to take weeks. The use of chemical weapons is clearly prohibited by the 1968 Chemical Weapons Convention, to which Ariana is a signatory. It is unclear why Ariana decided to violate international law at this time. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In Reyill, Atropian forces assisted by NATO forces reacted quickly to the attack, securing the areas affected and decontaminating persons exposed to the deadly attack, which infected dozens. It is unknown at this time if there are any fatalities as a result of the attack; nerve agents notoriously kill over a long period of time. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The Atropian Ambassador to the United Nations, Mr. Yashar Teymur Aliyev has filed protest against Ariana with the Director General of the United Nations in accordance with the UN Chemical Weapons Convention. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Stay tuned for continuing coverage of the war in Atropia.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "CBI_2267.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "24",
        "timestamp": "May-19-2015",
        "published": "0",
        "category": "local",
        "title": "Threat of Chemical Strikes Loom",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">NATO&rsquo;s Internation<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">al Security Force <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Terten Sea has been urged to deploy advanced ballistic missile defence systems as soon as possible, a spokesperson for the Ground Forces of the Republic of Atropia (GFRA) announced this morning. The GFRA asserts that the deployment of a Terminal High Altitude Area Defence System (THAAD) <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">an American defence asset <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">is necessary to protect against Arianian chemical strikes, which would most likely be delivered through missile attacks against Atropian territory. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;THAAD should be deployed <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">today to do what it was designed to do <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">protect and defend a large <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">regional area by tracking and defeating short and medium range incoming missiles,&rdquo; said the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">spokesperson. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Having suffered chemical weapon attacks during its own chequered history, Arianian officials frequently speak about the dangers such arms pose. The United States and Europe, however, have passed sanction to block companies from providing dual-use chemicals to Ariana. The latest assessments by a variety of chemical weapons watchdog organizations claim that in addition to a robust stockpile of chemical weapons capabilities, Ariana maintains the ability to produce chemical warfare agents, as well as the ability of weaponising chemical weapons agents in a variety of delivery systems. Although an option exists for states-parties to request a challenge inspection of alleged weapons sites under the terms of&nbsp;the Chemical Weapons Convention, no state-party, including the United States, has called for such an inspection in Ariana. And given Ariana&rsquo;s status as a regional belligerent, it seems unlikely the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">revolutionary Islamic theocracy would comply. <\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;With the known possession of chemical ballistic missiles by Ariana, the high risk of not being able to <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">eliminate all of them in a pre-emptive offensive strike and the possibility of an Arianian counterattack targeting nearby Atropian troops, NATO forces, and major city populations with Scud chemical missiles in retaliation, it would be irresponsible not to deploy a United States THAAD battery for the protection <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">of American and allied lives prior to the launching of an offensive strike,&rdquo; the spokesperson said.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "E1E2.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "23",
        "timestamp": "May-19-2015",
        "published": "0",
        "category": "local",
        "title": "Thousands of Nervous Atropians Queue for Gas Masks",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Thousands of Atropians were crowding gas-mask distribution facilities, readying for a potential chemical conflict with Ariana. Officers of the National Police of the Republic of Atropia were deployed to maintain order in the capital city Cela, where more than 5,000 people jostled in line as they waited for their protective kits. A sports arena there was being used as a distribution centre to accommodate the crowds. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The rising tension and long lines have led to some chaos. One local resident was critical of the distribution plans, saying, &quot;I don&#39;t think there should be only one spot where masks are being distributed. For over so many people in the area I think they should have provided at least ten different spots.&quot; Many locals are concerned for their safety, with one local resident saying that &quot;there is a panic because everyone says there is going to be an imminent attack either sometime today or over the weekend and no-one wants to be caught without a mask.&quot; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The masks were distributed as part of a program to assist the civilian population with its preparedness for the arrival of armed conflict to the area. Atropian officials, most notable President Dimetz himself, have expressed on multiple occasions their certainty that the Arianian invasion is explicitly intended to&nbsp;seize Atropian energy resources. If an attack on the capital is imminent &ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">officials have argued <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">then there is a possibility that innocent civilians could be exposed to Arianian chemical weapons. <\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">While similar gas-mask distribution programs are taking place in most cities of northern Atropia, a spokesperson for the office of President Dimetz admits that the present security crisis regretfully prevents such efforts from being carried out in the southern part of the country. In any case, she assured ASTV, the measure was purely precautionary and not intended to spread an unnecessary panic.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "E1E1.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "21",
        "timestamp": "May-17-2015",
        "published": "0",
        "category": "local",
        "title": "ICRC Addresses the War in Atropia",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In Atropia, the representative for the International Committee of the Red Cross (ICRC) expressed concern about the ongoing conflict on the Atropia-Ariana border and the potential for increased pressure on the already strained camps for internally displaced persons (IDPs) in Atropia. ICRC has been providing humanitarian assistance to IDP camps in Atropia since 1992. The ICRC representative reports that the camps are already stretched to capacity and the new fighting may cause unrest in communities <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">near the border which could lead to an increase in IDPs. &ldquo;The situation <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">is of great concern especially for the women and children who are particularly vulnerable. Even though we are entering the summer, the camps are ill equipped to provide shelter and supplies in variable conditions that can threaten the health and well-being of innocent people, which in turns undermines stability and risks throwing <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">southern Atropia into a human rights catastrophe,&rdquo; said Johanna Brown, Senior Field Advisor for ICRC. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">ICRC will continue to monitor the situation on the ground. The ICRC reminds everybody to respect the Red Cross emblem and the work of the ICRC staff and volunteers so that the ICRC can continue to assist those affected by the conflict.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D6D1_1.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "19",
        "timestamp": "May-16-2015",
        "published": "0",
        "category": "local",
        "title": "World Vision Reacts to Arianian Aggression Against Atropia",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">As news develops about the incursion of Arianian troops into Atropia, World Vision warns of the immediate humanitarian needs in the region will continue to grow. World Vision is gravely concerned about the impact of the escalating crisis on a generation of children. More than half of those needing assistance are children, many under direct threat of violence, says World Vision.&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;The international community is not doing enough. The children affected by <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">this new conflict need to become a priority for world leaders, particularly those with influence <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">over parties to the conflict,&rdquo; says World Vision&rsquo;s director of external relations, Chris Derksen Hiebert.&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">World Vision officials are also becoming concerned that the growing conflict will have a negative impact on the work that they have been doing in and <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">around Avsar. &ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Our Micro-finance arm, VisionFund International, is one of the foremost micro-loan providers in the world. It lends hard working people the small amount of capital they need to kick-start or expand their family businesses. We have just begun to see a positive impact in Noust<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rdquo;, said <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Holly Barnes<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">, regional representative for World Vision. &ldquo;If fighting escalates and the conflict continues, all of the ground that we have gained will be lost.&rdquo;&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The child-focused aid agency is calling for all parties to the conflict, supported by those states with influence over them, to urgently focus on reaching a peaceful agreement to end the bloodshed and take immediate steps to protect children and enable humanitarian access.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D5D1.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "12",
        "timestamp": "May-12-2015",
        "published": "0",
        "category": "local",
        "title": "Ariana Shoots Down Atropian Plane",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today, the Arianian military shot down a military plane of the Atropian Air Force while the aircraft was conducting missions within Atropian territory along its border with Ariana. The shoot-down marks the first time Arianian military forces have brought down an Atropian aircraft, and it raises new concerns for the UN-authorized NATO defensive campaign against the mounting Arianian threat to the Republic of Atropia. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The Tornado reconnaissance plane was flying in southern Atropia when the Atropian military lost touch with the aircraft. Witnesses on the ground state they saw large missiles strike the aircraft, which erupted into an explosion and scattered debris just north of Khachirli, Dayob Province. The pilot was seen ejecting from the plane, and Air Force officials confirm he is now being treated for injuries. The Arianian government admitted to shooting down the Atropian aircraft because it entered Arianian airspace <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">an accusation the Atropian military denies based on its observations of <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the plane&rsquo;s mission <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">and flight path.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D12.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "11",
        "timestamp": "May-12-2015",
        "published": "0",
        "category": "local",
        "title": "Ariana Invades Atropian Territory",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today the bellicosity of Ariana entered its final, terminal stage. Without any provocation, Ariana <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">declared that it was taking &ldquo;security actions&rdquo; to secure its own territory by invading Atropia. At the same <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">time the announcement was being made, Ariana launched a missile attack on targets in Atropia. This is only further evidence that Ariana had been planning this bald-faced attack for months if not years. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Arianian forces, which had been positioned on the Atropian border for months in blatant disregard for United Nations Security Council Resolutions 2193 and 2517, then began to advance across the international boundary. Although busy directing Atropian and NATO defenses, President <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Demetz&rsquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">office <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">issued the following statement to uplift the hearts of the Atropian people: &ldquo;The bright flame of Atropian <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">democracy and prosperity will never be extinguished. Despite the size of Ariana, and the corrupt injustice with which its dictators seek to enslave the region, they are no match for Atropia, and our allies in NATO. With the might of the Atropian military, the support of our NATO allies, and the weight of the United Nations behind us, Arianian forces will shortly be destroyed. In this time of difficulty, we must <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">remain united in defense of our proud republic!&rdquo;&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D11.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "10",
        "timestamp": "May-05-2015",
        "published": "0",
        "category": "local",
        "title": "Atropian Forces Move to Reinforce Border",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today it was announced that as part of regularly scheduled unit rotations, and in view of the worsening situation along the Arianian border, an armoured brigade would be moving to the border area to replace an infantry brigade. A spokesman at Ground Forces of the Republic of Atropia Headquarters in Cela stated that in conjunction with long-standing rotational schedules, it was determined that the redeploying 348th Motorized Infantry Brigade would be replaced with the 354th Armoured Brigade. This will have the benefit of bringing greater fire-power to the border area, prompting Ariana to rethink its increasing bellicose posturing and rhetoric. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">While some opposition activists and Arianian media outlets have attempted to paint the move as an offensive one, nothing could be further from the truth.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D10.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "9",
        "timestamp": "May-05-2015",
        "published": "0",
        "category": "local",
        "title": "SAPA Insurgents Attack Police, Damage Power Station",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Pasron City today an insurgents claiming to be members of the &ldquo;South Atropian People&rsquo;s Army&rdquo;, or &ldquo;SAPA,&rdquo; caused significant damage the city&rsquo;s power plant, causing intermittent power outages <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">throughout the morning and forcing officials to increase power output to the city from the Trata natural gas power plant in Danye. While estimates of physical damage have not yet been tabulated, officials indicate no one was seriously injured or killed in the attack. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Pasron Provincial Governor Levant Vahid condemned the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">attack, stating, &ldquo;Today guerillas seeking to <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">undermine Atropia and frighten the people callously attacked the public infrastructure in Pasron City. Although the attack was swiftly and thoroughly crushed, it serves as a reminder that there are those forces inside as well as outside the country who would seek to rob Atropia of her freedoms. We must remain vigilant to these threats and defend against them if we are to remain a people free of terrorism, <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">separatism, and fear.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">It is widely believed that the SAPA forces were targeting the Pasron City Police Headquarters with mortar fire, but the rounds impacted the power generation facility. This has resulted in a loss of electrical power in the city. The local government is unsure when power will be restored.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D9.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "8",
        "timestamp": "May-04-2015",
        "published": "0",
        "category": "local",
        "title": "Ariana Lays Claim to Atropian Oil Reserves",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In a bold statement from the Arianian Foreign Ministry, Ariana has stated that its claim to the oil reserves of Atropia could no longer be ignored. Due to its continuing isolation from overseas markets, Ariana has struggled to come out of the global economic downturn. It had been hoped in international policy circles that the continuing difficulties in the non-extractive markets of Ariana would prompt a wave of economic liberalization, leading to political liberalization. Only three months ago, U.S. Ambassador to the United Nations Samantha Power stated, &quot;Arianians are finally beginning to realize that in order to achieve economic progress, there must be political progress; free elections, an opaque judicial system, and a freedom from tyranny.&quot; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The latest statement from Tehran, however, seems to dash this hope. Atropian Foreign Minister Azim Husainov appeared flummoxed in response to the statement by Ariana, stating that &quot;bullying and bluster have no place among civilized nations,&quot; and that any threats to Atropian sovereignty would be met with force.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D8.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "7",
        "timestamp": "April-14-2015",
        "published": "0",
        "category": "local",
        "title": "NATO\u2019s International Security Force Deploys to Cela",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The long awaited arrival of NATO forces has begun with the deployment of the famed International Security Force <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Terten Sea, or ISF-TS. The people of Ariana are warned to defy their warmongering <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">leaders&rsquo; foolish course to defy such modern military conventions. Emphasizing again the sovereignty of <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the Atropian nation in all its dealings, senior Ground Forces of the Republic of Atropia commanders will be assigned to this ISF-TS to reaffirm that it is Atropia leading the defence of its territory. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Likewise, a Status of Forces Agreement, with the most complimentary terms for Atropia and its government, has been signed. These measures serve to defy those cynics who claim that the Atropian government under the leadership of President Dimetz <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">is ceding Atropia&rsquo;s sovereignty and national <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">honour to foreigners in order to remain in power. On the contrary, it is the esteem that NATO and the United States holds for President Dimetz that has lead them to unquestioningly accept terms favourable to the government of Atropia. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The people of Atropia are strongly encouraged to denounce any negative comments directed against the government in these national security matters and report them to the police as crimes of sedition&nbsp;against the State. Also, the people are encouraged to show their appreciation for our NATO allies and display the hospitality, which has made our nation the envy of our more uncultured neighbours.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D7.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "6",
        "timestamp": "April-14-2015",
        "published": "0",
        "category": "local",
        "title": "NATO Establishes International Security Force \u2013 Terten Sea to Defend Atropia",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Acting quickly on NATO&rsquo;s authorization for the defense of Atropia and the punishing of Arianian <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">aggressors in accordance with UN Resolutions and international law, the United States European Command today has established the International Security Force <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Terten Sea. Atropian military experts familiar with the composition of such a force state that at this time it is unclear how long it will take to <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">force Ariana to cease its aggression. Now that both NATO&rsquo;s combined and joint forces are involved, the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Government of Atropia advises Ariana to abandon all claims on our territory, or face utter annihilation. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The people of A<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">tropia should be thankful for the Atropian government&rsquo;s wise leadership not just in <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">domestic policies, but in our foreign relations as well, which has secured the creation of an International Security Force so soon after Atropia first issued its call for the international community to rally to our defence. The international response seems to reflect the reality that we lead the defence not just of our homeland, but of Europe against the Arianian despotism. President <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Dimetz&rsquo; greatest legacy may be his <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">ability to secure the support of the greatest military alliance in history even though Atropia is not yet a formal member.&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">While Atropia will undoubtedly be offered membership to NATO on the most favourable of terms once Ariana is subdued, government leaders say we must now concentrate on our efforts to defend the security of the Atropian people.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D6.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "5",
        "timestamp": "April-02-2015",
        "published": "0",
        "category": "local",
        "title": "Government Closes Borders with Ariana",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The Government closed all border crossings with Ariana on Monday, following Ariana&rsquo;s repeated violations of Atropian sovereignty with Ariana&rsquo;s use of unprovoked cross<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">-border attacks. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">This is yet one more eruption <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">in the region sponsored by Ariana&rsquo;s aggressive hegemonic designs against <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the Atropian government. Only weeks ago, Ariana fired missiles into Atropian waters in the Terten Sea. If Ariana is not able to respect the international borders of this sovereign nation, Atropia is prepared to take all steps necessary to defend its territory. Atropia has very strong allies from across Europe and the US and is prepared to defend itself.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D5.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "4",
        "timestamp": "March-10-2015",
        "published": "0",
        "category": "business",
        "title": "U.S. Passes Sanctions Against Ariana; EU Considers Sanctions",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The State Department of the United States of America released a statement today announcing that sanctions were to be imposed on Ariana in response to Ariana&#39;s aggressive moves towards Atropia. Atropia, long the U.S. ally in the region, has been facing an increasingly hostile Ariana in the last year. Ariana accused Atropia of threatening its &quot;interests,&quot; and shortly afterward conducted large-scale military exercises along the Atropian border designed to intimidate its neighbor. Then, Arianian forces fired on Atropian border forces across the border, and launched a series of audacious claims to Atropia&#39;s resources and threats against Atropia&#39;s allies, including the U.S. The sanctions will target Arianian export and finance sectors, as well as nations that provide Ariana with military equipment. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">It is reported that the European Union is similarly formulating a sanctions plan against Ariana.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D4.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "3",
        "timestamp": "February-07-2015",
        "published": "0",
        "category": "local",
        "title": "Arianian Forces Fire Across Border Into Atropia, Atropian Forces Respond",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In an unprovoked action, Arianan military forces have fired a mortar across the border into Atropia. The mortar round landed in Velaz Province, resulting in some property damage. Investigators from Cela are currently in the area to make a formal damage assessment to present to the Arianian government and appropriate international bodies. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Atropian forces returned fire in order to uphold Atropian sovereignty. According to the Arianian government, one Arianian soldier was killed in the counter-barrage. Foreign Minister Azim Husainov <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">spoke with reporters late last night, saying, &ldquo;Arianian forces, unprovoked, fired upon our territory today. <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">We do not know yet why this was done, although we hope this is not indicative of Arianian intentions or character. Atropian forces defended themselves well, and we believe one Arianian was killed in the counter-<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">barrage.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Ariana has become increasingly aggressive towards Atropia of late.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D3.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "2",
        "timestamp": "November-05-2014",
        "published": "0",
        "category": "political",
        "title": "TPP Claims Government Denied Visa to Political Activist Out of Spite",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The Atropian government confirmed this week that it has denied an entry visa to Srda Popovic, a Serbian biologist, political activist, and executive director of the Centre for Applied Nonviolent Action and Strategies (CANVAS). TPP had invited Popovic to speak at its <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">South Atropian<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">political caucus in Pasron City later this year. Reacting strongly to the announcement, Poblatioff Mathu, the leader of TPP, <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">released a statement today that, &ldquo;If the Atropian government believes in the rule of law, it s<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">hould not <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">be hindering advocacy of claims against its agents for wrongful death and injury.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">In 2003, Popovic CANVAS as an organization that advocates for the use of nonviolence resistance to promote human rights and democracy. Established in Belgrade, CANVAS has worked with prodemocracy activists from more than 50 countries, including Ariana, Zimbabwe, Burma, Venezuela, Ukraine, Gorgas, Palestine, Western Sahara, West Papua, Eritrea, Belarus, and Tonga and, recently, Tunisia and Egypt. In 2006, Popovic and <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">two other CANVAS members authored a book called &lsquo;Nonviolent Struggle: 50 Crucial <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Points, a how-<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">to guide for nonviolent struggle.&rsquo; In November 2011 Popovic was one of the speakers at the TEDxKrakow conference. Between December 2011 and February 2013, the video of his speech at ted.com received almost 200,000 views.&nbsp;<\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The TPP claims that at stake for the Atropian government is the exposure of a false narrative the government had used to justify what the TPP calls the <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">persecution of political dissent.<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">&rdquo; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">As Popociv <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">described it in his response to the denial of his visa, &ldquo;Non<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">-violent political activism, such as that carried <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">out by the loyal members of the South Atropian People&rsquo;s Party in solidarity with other similar political <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">organizations all over the civilized world, <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">disrupts the narrative that &lsquo;law enforcement&rsquo; against &lsquo;political subversion&rsquo; is an unqualified success toward development at minimal cost to civil libe<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">rties. The RPD in general, and President Dimetz in particular, does not want anyone challenging their spree of terror, but <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the Atropian people should have the right to know.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The government staunchly denied the suggestion that its refusal to allow Popovic a visa to enter Atropia has anything to do with politics. Instead, a government spokesperson, the move was intended to protect <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Popovic from being a target of attacks by the insurgent South Atropian People&rsquo;s Army &ndash; <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">a violent anti- government terrorist organization who has a history of targeting high profile public speakers. In reference to the value that such a speaker could bring to the common Atropian voter, a spokesperson <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">for the Republican Party of Democracy (RPD) stated that, &ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">the RPD government welcomes any opportunity for the people to decide whether the true benefits of supporting President Dimetz and the RPD is indeed worth it. Popovic really has nothing to do with that conversation, which is between <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Atropians.&rdquo;&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D2.png",
        "media_type": "image",
        "media_quote": ""
    },
    {
        "id": "1",
        "timestamp": "September-06-2014",
        "published": "0",
        "category": "local",
        "title": "Ariana Begins Large-Scale Military Exercise Along Atropian Border",
        "body": "<div class=\"page\" title=\"Page 1\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Today, Ariana began a military exercise near the Atropian border. In Velaz, Arianian infantry, artillery, and tanks could be seen moving in formation less than three kilometres from Atropian territory. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Many believe the Arianian exercise is designed to intimidate Atropia into giving up recent claims to oil resources in the Terten Sea. University of Cela International Centre Director Professor Efendi Bakayev <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">stated &ldquo;<\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Arianian intentions are clear. They want to bully Atropia into submission, and strip away whatever riches they can carry. What is unclear is Ariana&#39;s motivation. Atropia has always sought to maintain civil relations with its neighbours. If Ariana is doing this simply for domestic political reasons, I <\/span><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">think it will backfire.&rdquo; <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The exercises are expected to last ten days. But this is no exercise, rather the use of military muscle, a deliberate provocation and the latest in a series of such manoeuvres by Ariana aimed at intimidation and destabilization. <\/span><\/p>\n\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">The days when Ariana conducted military exercises or played war games simply to get their troops in a state of readiness have long gone. The manoeuvres in northern Ariana were announced soon after test&nbsp;missiles were launched into the Terten Sea. Both are aimed at heightening tension. Other countries also use military exercises and war games to send messages, exert pressure or to issue threats. The US sends its fleet all round the world, from the Persian Gulf to the South China Sea, and North Korea conducts regular war games, and there is nothing playful about them. <\/span><\/p>\n\n<div class=\"page\" title=\"Page 2\">\n<div class=\"layoutArea\">\n<div class=\"column\">\n<p><span style=\"font-size: 11.000000pt; font-family: 'Calibri'\">Military exercises date back to at least Frederick the Great in the 18th century. He is famous for the repeated drilling that made the Prussian army so formidable. But Ariana has made military exercises part of the campaign, a &quot;disguised warfare&quot; strategy that sees troops massed along the border combined with stirring unrest among Arianian sympathizers, likely backed by Arianian Special Purpose Forces.&nbsp;<\/span><\/p>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n<\/div>\n",
        "media_filename": "D1D1.png",
        "media_type": "image",
        "media_quote": ""
    }
]});
