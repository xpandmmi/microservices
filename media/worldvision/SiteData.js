'use strict';

angular.module('gnnApp')
	.service('SiteData', function Sitedata() {
		return 	[
    {
        "id": "1",
        "timestamp": "May-07-2015",
        "published": "0",
        "category": "local",
        "title": "TEST ARTICLE",
        "body": "<p>TEST ARTICLE<\/p>\n",
        "media_filename": "",
        "media_type": "image",
        "media_quote": ""
    }
]});