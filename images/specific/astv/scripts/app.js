'use strict';

var app = angular.module('gnnApp', ['ngSanitize'])
	.filter('StartIndex', function(){
		return function(input, start){
			start = +start;
			return input.slice(start);
		};
	})
	.filter('blurbIt', function(){
		return function(input, length){
			input = input.substring(input.indexOf('<p>'), input.indexOf('</p>'));//.toString().split(' ');
			return input;
			// var blurb = [];
// 			for (var i=0; i<length; i++){
// 				blurb.push(input[i]);
// 			}
// 			return blurb.join(' ') + '...';
		}
	})
	.value('media_url', '../../media/astv');