'use strict';

angular.module('gnnApp')
  .controller('MainCtrl', ['$scope', '$location', 'SiteData', 'Slider', 'media_url', function ($scope, $location, SiteData, Slider, media_url) {
	  $scope.media_url = media_url;
	  $scope.currentPage = 0;
	  $scope.pageLimit = 8;

	  if (window.location.href.split('?').length > 1){
          var category = window.location.href.split('?')[1].substring(4);
		  var articles = [];
		  for (var i=0; i<SiteData.length; i++){
			  if (SiteData[i].category == category){
				  articles.push(SiteData[i]);
			  }
		  }
		  $scope.articles = articles;
		  $scope.header = articles[0].category;
	  } else {
		  var _siteData = [];
		  var _localData = [];
		  var _regionalData = [];
		  var _worldData = [];
		  
		  for (var i=0; i<SiteData.length; i++){
			if (SiteData[i].category == 'local'){ _localData.push(SiteData[i]); }
			if (SiteData[i].category == 'politics'){ _regionalData.push(SiteData[i]); }
			if (SiteData[i].category == 'world'){ _worldData.push(SiteData[i]); }
			_siteData.push(SiteData[i]);
		  }
		  
		  $scope.articles = _siteData;
		  $scope.articlesLocal = _localData[0];
		  $scope.articlesPolitics = _regionalData[0];
		  $scope.articlesWorld = _worldData[0];
		  $scope.header = 'Articles';
	  }
	  $scope.slider = Slider;
	  
	  // Methods
	  $scope.ShowArticle = function(index){ $scope.article = SiteData[index]; };
	  
	  $scope.ReadArticle = function(index){
		  window.location = "article.html?id=" + index;
	  };
	  
	  // Search
	  $scope.query = '';
	  
	  // Pagenation
	  $scope.GotoPrev = function(){ $scope.currentPage--; };
	  $scope.GotoNext = function(){ $scope.currentPage++; };
	  $scope.GotoPage = function(index){ $scope.currentPage = index; };
	  $scope.isFirst = function(){ return $scope.currentPage === 0; };
	  $scope.isLast = function(){ return $scope.currentPage >= $scope.articles.length/$scope.pageLimit -1; };
	  $scope.NumberOfPages = function(){
		  var pages = [];
		  var count = Math.ceil($scope.articles.length/$scope.pageLimit);
		  for (var i=0; i<count; i++){ pages.push(i+1); }
		  return pages;
	  };
	  
	  // Support 
	  $scope.isActive = function(index){
		  if (index){
			  return 'active'
		  };
	  }
	  
	  // Init
	  $scope.ShowArticle(0);
	  
  }]);