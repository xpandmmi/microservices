'use strict';

angular.module('gnnApp')
  .controller('MainCtrl', ['$scope', 'SiteData', 'media_url', function ($scope, SiteData, media_url) {
	  $scope.currentPage = 0;
	  $scope.pageLimit = 6;
	  $scope.media_url = media_url;
	  $scope.articles = SiteData;
//	  $scope.newsletters = Newsletters;
	  
	  // Methods
	  $scope.ShowArticle = function(index){
		  $scope.article = SiteData[index];
	  };
	  
	  $scope.ReadArticle = function(index){
		  if (window.location.toString().indexOf('pages') > 0){
			  window.location = "../article.html?id=" + index;
		  } else {
			  window.location = "article.html?id=" + index;
		  }
		  //
	  };
	  
	  // Search
	  $scope.query = '';
	  
	  // Pagenation
	  $scope.GotoPrev = function(){
		  $scope.currentPage--;
	  };
	  $scope.GotoNext = function(){
		  $scope.currentPage++;
	  };
	  $scope.GotoPage = function(index){
		  $scope.currentPage = index;
	  };
	  $scope.isFirst = function(){
		  return $scope.currentPage === 0;
	  };
	  $scope.isLast = function(){
		  return $scope.currentPage >= $scope.articles.length/$scope.pageLimit -1;
	  };
	  $scope.NumberOfPages = function(){
		  var pages = [];
		  var count = Math.ceil($scope.articles.length/$scope.pageLimit);
		  for (var i=0; i<count; i++){
			  pages.push(i+1);
		  }
		  return pages;
	  };
	  
	  // Init
	  $scope.ShowArticle(0);
	  
  }]);