'use strict';

angular.module('gnnApp')
    .directive('mediaMultiplayer', function () {
        return {
            template: '<div></div>',
            restrict: 'E',
            replace: true,
            link: function(scope, element, attrs){
                function renderDirective(file_name, file_type){
                    if (file_name == undefined){
                        file_name = attrs.placeholder;
                    }
                    if (file_type == 'image'){
                        element.html('<img src="assets/article-images/' + file_name  + '" width="620" class="pull-left" style="margin-right:10px;" />');
                    }
                    if (file_type == 'video'){
                        element.html('<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="620" height="348" id="FLVPlayer"><param name="movie" value="FLVPlayer_Progressive.swf" /><param name="quality" value="high"><param name="wmode" value="opaque"><param name="scale" value="noscale"><param name="salign" value="lt"><param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Corona_Skin_2&amp;streamName=assets/article-videos/' + file_name + '.flv&amp;autoPlay=false&amp;autoRewind=false" /><param name="swfversion" value="15,0,0,0"><object type="application/x-shockwave-flash" data="FLVPlayer_Progressive.swf" width="620" height="348"><param name="quality" value="high"><param name="wmode" value="opaque"><param name="scale" value="noscale"><param name="salign" value="lt"><param name="FlashVars" value="&amp;MM_ComponentVersion=1&amp;skinName=Corona_Skin_2&amp;streamName=assets/article-videos/' + file_name + '.flv&amp;autoPlay=false&amp;autoRewind=false" /><param name="swfversion" value="15,0,0,0"><param name="expressinstall" value="scripts/expressInstall.swf"></object></object>');
                    }
                    if (file_type == 'audio'){
                        element.html('<audio controls><source src="assets/article-audio/' + file_name + '.mp3" type="audio/mpeg">Your browser does not support the audio element.</audio>');
                    }
                }

                renderDirective(attrs.url, attrs.type);

                scope.$watch(attrs.url, function (newVal){
                    renderDirective(newVal, attrs.type);
                });
            }
        };
    });