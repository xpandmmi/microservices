'use strict';

angular.module('gnnApp')
  .controller('ArticleCtrl', ['$scope', 'SiteData', 'media_url', function ($scope, SiteData, media_url) {
	  $scope.currentPage = 0;
	  $scope.pageLimit = 6;
	  $scope.articles = SiteData;
	  $scope.media_url = media_url;
	  
	  // Methods
	  $scope.ShowArticle = function(index){
		  for (var i=0; i<SiteData.length; i++){
			  if (SiteData[i].id == index){
				  $scope.article = SiteData[i];
			  }
		  }
	  };
	  
	  $scope.ReadArticle = function(index){
		  window.location = "article.html?id=" + index;
	  };
	  
	  // Search
	  $scope.query = '';
	  
	  // Init
	  $scope.ShowArticle(window.location.search.substring(4));	  
  }]);